﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassKaks
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 3;
            int b = 7;
            Prooviks.Swap(ref a, ref b);
            Console.WriteLine($"a={a} ja b={b}");

            string nimi = "Toomas";
            string pereNimi = "Linnupoeg";
            Prooviks.Swap(ref nimi, ref pereNimi);
            Console.WriteLine(nimi);

            int s=0; int k=0;
            Prooviks.Arvuta(a, b, ref s,ref k);
            Console.WriteLine($"arvude {a} ja {b} summa on {s} ning korrutis {k}");

            int[] neliArvu = { 4, 7, 0, 0 };
            Prooviks.Arvuta(neliArvu[0], neliArvu[1], ref neliArvu[2], ref neliArvu[3]);
            foreach (var x in neliArvu) Console.WriteLine(x);

            Inimene i1 = new Inimene("Henn", "Sarv")
            {
                Vanus = 63
            };

            Console.WriteLine(i1.TäisNimi(", "));

            Inimene i2 = new Inimene("", "Saarlane")
            {
                Vanus = 28
            };

            Inimene i3 = new Inimene            {
                EesNimi = "kalle",
                PereNimi = "Kulles",
                Vanus = 10
            };
            i3.Vanus += 5;
            i3.setVanus(i3.getVanus() + 5);

            Console.WriteLine(i3.Vanus);

            // suht jabur asi
            Inimene i4 = new Inimene("JUTA", "JUURIKAS") {}; 

            foreach(var i in Inimene.Inimesed)
                Console.WriteLine(i);

            i2.EemaldaNimekirjast();

            foreach (var i in Inimene.Inimesed)
                Console.WriteLine(i);

        }
    }

    
  


}
