﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassKaks
{
    class Prooviks
    {
        public static void Vaheta(ref int üks, ref int teine)
        {
            int x = üks;
            üks = teine;
            teine = x;
        }

        // generic meetod
        public static void Swap<T>(ref T üks, ref T kaks)
        {
            T x = üks;
            üks = kaks;
            kaks = x;
        }

        public static void Arvuta(int üks, int kaks, ref int summa, ref int korrutis)
        {
            summa = üks + kaks;
            korrutis = üks * kaks;
        }

    }

}
