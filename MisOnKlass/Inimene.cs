﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MisOnKlass
{
    class Inimene
    {
        public static List<Inimene> Inimesed = new List<Inimene>();
        public static int InimesteArv = 0;

        public int Number = ++InimesteArv;
        public string Nimi;
        public DateTime SünniAeg;
        public Sugu Sugu;

        public Inimene() : this("nimeta")
        {
            Console.WriteLine("Tehtud");
        }

        public Inimene(string nimi) // parameetriga konstruktor
        {
            Nimi = nimi;
            this.Number = ++InimesteArv;
            Inimesed.Add(this);
        }

        public int AnnaVanus()
        {
            int d = DateTime.Now.Year - this.SünniAeg.Year;
            if (SünniAeg.AddYears(d) > DateTime.Now) d--;
            return d;
        }

        public static int AnnaVanus(Inimene kelle)
        {
            int d = DateTime.Now.Year - kelle.SünniAeg.Year;
            if (kelle.SünniAeg.AddYears(d) > DateTime.Now) d--;
            return d;
        }


        public int Vanus => AnnaVanus();
   

        public override string ToString() => $"{Number}.  {Sugu} nimega {Nimi} ja on {Vanus} aastane";
        

    }
}
