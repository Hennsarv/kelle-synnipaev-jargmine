﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MisOnKlass
{


    class Program
    {
        static void Main(string[] args)
        {


            Inimene henn = new Inimene("Henn")
            {
                SünniAeg = DateTime.Parse("7.3.1955"),
                Sugu = Sugu.Mees
            };
            var peeter = new Inimene()
            {

                Nimi = "Peeter",
                Sugu = Sugu.Mees,
                SünniAeg = new DateTime(1960, 4, 18)
            };


            var malle = new Inimene("Malle")
            {
                Sugu = Sugu.Naine,
                SünniAeg = new DateTime(1990, 4, 18)
            };



            foreach (var x in Inimene.Inimesed)
            {
                Trüki(x);
            }

            Trüki(malle.AnnaVanus());  // objekti e. instantsi funktsioon
            Trüki(Inimene.AnnaVanus(peeter));   // staatiline funktsioon
            Trüki(Inimene.InimesteArv);     // staatiline väli
            Trüki(peeter.Number);    // objekti e. instantsi väli

            Trüki((new Inimene("Kaarel")).Number);

            Inimene.Inimesed[3].Nimi = "Toots";

            Trüki(Inimene.Inimesed[3].Nimi);

            Trüki(Inimene.Inimesed[3].AnnaVanus());
            Trüki(Inimene.AnnaVanus(Inimene.Inimesed[3]));


        }

        static void Trüki()
        {
            Console.WriteLine();
        }

        static void Trüki(object o) // meetod
        {
            Console.WriteLine(o);
        }

        static string Küsi(string küsimus)   // fuktsioon
        {
            Console.Write(küsimus + ": ");
            return Console.ReadLine();
        }



        static double Liida(int üks, int veelüks)
        {
            return üks + veelüks;
        }


        static int Liida(int üks, int kaks, params int[] kolm)
        {
            return üks + kaks + kolm.Sum(); ;
        }


    }


}
