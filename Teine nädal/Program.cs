﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Teine_nädal
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = @"..\..\Nimekiri.txt";
            string[] loetudRead = File.ReadAllLines(filename);

            string kogufail = File.ReadAllText(filename);
            Console.WriteLine(kogufail);

            string[] teisedread = kogufail.Split('\n');
            foreach (var x in teisedread) Console.WriteLine(x);

            //foreach (var x in loetudRead) Console.WriteLine(x);
            Dictionary<string, DateTime?> nimekiri = new Dictionary<string, DateTime?>();
            foreach (var loetudRida in loetudRead)
            {
                string[] osad = loetudRida.Split(',');
                string nimi = osad[0];
                DateTime? sünniaeg = null;
                if (osad.Length > 1)
                {
                    DateTime.TryParse(osad[1], out DateTime sa);
                    if (sa.Year > 1900 && sa < DateTime.Now) sünniaeg = sa;
                }
                nimekiri.Add(nimi, sünniaeg);
            }

            foreach(var x in nimekiri.Keys)
            {
                Console.WriteLine($"Inimene {x} sünnipäev on {nimekiri[x]?.ToLongDateString()}");
            }

            //DateTime v = DateTime.Now;
            //foreach (var x in nimekiri.Values)
            //    if (x.HasValue &&  x < v) v = x.Value;
            //Console.WriteLine($"Vanim sünnipäev on {v}");
            //Console.WriteLine($"ka niimoodi {nimekiri.Values.Min()}");

            DateTime v = DateTime.Now.Date.AddYears(1);
            string kellel = "";
            foreach(var x in nimekiri)
            {
                if (x.Value.HasValue)
                {
                    var d = x.Value.Value;
                    int m = DateTime.Now.Year - d.Year;
                    d = d.AddYears(m);
                    if (d < DateTime.Now.Date) d = d.AddYears(1);
                    if (d < v) { v = d; kellel = x.Key; }
                }
            }
            Console.WriteLine(
            $"järgmine sünnipäevalaps on {kellel}, " +
            $"kellel on {v.ToShortDateString()} sünnipäev ");
        }
    }
}
